﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejercicio_3_Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double num1 = double.Parse(textBox1.Text);
                double num2 = double.Parse(textBox2.Text);
                double r = num1 + num2;
                textBox4.Text = r.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor usar números enteros o decimales");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                double num1 = double.Parse(textBox1.Text);
                double num2 = double.Parse(textBox2.Text);
                double r = num1 - num2;
                textBox4.Text = r.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor usar números enteros o decimales");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                double num1 = double.Parse(textBox1.Text);
                double num2 = double.Parse(textBox2.Text);
                double r = num1 * num2;
                textBox4.Text = r.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor usar números enteros o decimales");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                double num1 = double.Parse(textBox1.Text);
                double num2 = double.Parse(textBox2.Text);
                double r = num1 / num2;
                textBox4.Text = r.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Favor usar números enteros o decimales");
            }
        }
    }
}
